package edu.school21.sockets.exceptions;

public class NotSavedSubEntityException extends Throwable {
    public NotSavedSubEntityException(Throwable cause) {
        super("Error: нет такого user_id или author_id\n" + cause);
    }
}