package edu.school21.sockets.repositories;

import edu.school21.sockets.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class UsersRepositoryImpl extends JdbcTemplate implements UsersRepository {

    @Autowired
    public void setRepository(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    @Override
    public User findById(Long id)  {
        String sql = "select * from chat.userTable where id = ?";
        return super.queryForObject(sql, (rs, rowNum) ->
                new User(
                        rs.getLong("id"),
                        rs.getString("login"),
                        rs.getString("password")
                ), id);
    }

    @Override
    public Optional<User> findByLogin(String login)  {
        try {
            String sql = "select * from chat.userTable where login = ?";
            User user = super.queryForObject(sql, (rs, rowNum) ->
                    new User(
                            rs.getLong("id"),
                            rs.getString("login"),
                            rs.getString("password")
                    ), login);
            return (Optional.ofNullable(user));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<User> findAll()  throws SQLException {
        String sql = "select * from chat.userTable";
        return super.query(
                sql,
                (rs, rowNum) ->
                        new User(
                                rs.getLong("id"),
                                rs.getString("email"),
                                rs.getString("password")
                        )
        );
    }

    @Override
    public void save(User user)  throws SQLException  {
        String insertSql;
        insertSql = "insert into chat.userTable(login, password) VALUES (?, ?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        super.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(insertSql, new String[] { "id" });
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPassword());
            return ps;
        }, keyHolder);
    }

    @Override
    public void update(User user) throws SQLException {
        super.update(
                "update chat.userTable set login = ? where id = ?",
                user.getLogin(), user.getId());
    }

    @Override
    public void delete(Long id) throws SQLException {
        super.update(
                "delete from chat.userTable where id = ?",
                id);
    }
}
