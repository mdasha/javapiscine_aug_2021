package edu.school21.sockets.services;

import edu.school21.sockets.exceptions.NotSavedSubEntityException;
import java.sql.SQLException;

public interface UsersService {
    boolean signUp(String login, String password) throws SQLException, NotSavedSubEntityException;
}
