package edu.school21.sockets.app;

import edu.school21.sockets.server.Server;

public class Main {
    public static void main(String[] args) {
        String [] args0 = args[0].split("=");
        int serverPort = Integer.parseInt(args0[1]);
        Server server = new Server(serverPort);
        server.startServer();
    }
}
