package edu.school21.sockets.server;

import edu.school21.sockets.config.SocketsApplicationConfig;
import edu.school21.sockets.exceptions.NotSavedSubEntityException;
import edu.school21.sockets.services.UsersService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;

public class Server {
    private final int serverPort;
    private Socket clientSocket = null;
    private ServerSocket serverSocket = null;
    private BufferedReader in = null;
    private BufferedWriter out = null;

    public Server(int serverPort) {
        this.serverPort = serverPort;
    }

    public void startServer() {
        try {
            serverSocket = new ServerSocket(serverPort);
            clientSocket = serverSocket.accept();
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            out.write("Hello from Server!\n");
            out.flush();
            String startMsg = in.readLine();
            System.out.println(startMsg);
            out.write("Enter username:\n");
            out.flush();
            String userName = in.readLine();
            System.out.println(userName);
            out.write("Enter password:\n");
            out.flush();
            String password = in.readLine();
            System.out.println(password);
            ApplicationContext context = new AnnotationConfigApplicationContext(SocketsApplicationConfig.class);
            UsersService usersService = context.getBean(UsersService.class);
            if (usersService.signUp(userName, password)) {
                out.write("Successful!\n");
            }
            else {
                out.write("The user with name: " + userName + " has already been registered!\n");
            }
            out.flush();
        } catch (IOException | SQLException | NotSavedSubEntityException e) {
            e.printStackTrace();
        } finally {
            try {
              in.close();
              out.close();
              serverSocket.close();
              clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
