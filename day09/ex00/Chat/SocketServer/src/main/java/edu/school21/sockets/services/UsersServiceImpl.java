package edu.school21.sockets.services;

import edu.school21.sockets.exceptions.NotSavedSubEntityException;
import edu.school21.sockets.models.User;
import edu.school21.sockets.repositories.UsersRepository;
import edu.school21.sockets.repositories.UsersRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService {
    private UsersRepository usersRepository;
    private PasswordEncoder passwordEncoder;

    public UsersServiceImpl() {}

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUsersRepository(UsersRepositoryImpl usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public boolean signUp(String login, String password) throws SQLException {
        Optional<User> user = usersRepository.findByLogin(login);
        if (user.isPresent()) {
            return false;
        }
        usersRepository.save(new User(0L, login, passwordEncoder.encode(password)));
        return true;
    }
}
