DROP SCHEMA IF EXISTS chat CASCADE;
CREATE SCHEMA chat;

create table if not exists chat.userTable
(
    id SERIAL PRIMARY KEY,
    login  VARCHAR(50) NOT NULL,
    password VARCHAR(150) NOT NULL
    );