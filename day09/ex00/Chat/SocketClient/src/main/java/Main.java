import java.io.*;
import java.net.Socket;

public class Main {
    private static BufferedReader in = null;
    private static BufferedWriter out = null;
    private static Socket clientSocket = null;

    public static void main(String[] args) {
        try {
            String [] args0 = args[0].split("=");
            int port = Integer.parseInt(args0[1]);
            clientSocket = new Socket("localhost", port);
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            String serverMsg = in.readLine();
            System.out.println(serverMsg);

            String signUpCmd = reader.readLine();
            out.write(signUpCmd + "\n");
            out.flush();

            String serverEnterName = in.readLine();
            System.out.println(serverEnterName);

            String userName = reader.readLine();
            out.write(userName + "\n");
            out.flush();

            String serverEnterPassword = in.readLine();
            System.out.println(serverEnterPassword);

            String password = reader.readLine();
            out.write(password + "\n");
            out.flush();

            String serverResult = in.readLine();
            System.out.println(serverResult);
        } catch (IOException e) {
            System.err.println(e);
        } finally {
            try {
                clientSocket.close();
                in.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
