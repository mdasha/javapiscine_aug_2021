package edu.school21.sockets.repositories;

import edu.school21.sockets.models.Message;

import java.sql.SQLException;
import java.util.Optional;

public interface MessagesRepository extends CrudRepository<Message>{
    Optional<Message> findByMessage(String message) throws SQLException;
}
