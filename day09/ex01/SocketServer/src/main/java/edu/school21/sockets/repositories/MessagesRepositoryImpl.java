package edu.school21.sockets.repositories;

import edu.school21.sockets.models.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;

@Repository
public class MessagesRepositoryImpl extends JdbcTemplate implements MessagesRepository {
    private final String      tableName;
    private final RowMapper<Message> ROW_MAPPER;

    @Autowired
    @Qualifier("dataSource")
    public void setRepository(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    public MessagesRepositoryImpl() {
        this.tableName = "message";
        this.ROW_MAPPER = (ResultSet resultSet, int rowNum) -> {
            return new Message(resultSet.getLong("id"), resultSet.getString("sender"), resultSet.getString("message"), resultSet.getTimestamp("time").toLocalDateTime());
        };
    }

    @Override
    public Optional<Message> findById(Long id)  {
        try {
            Message message= super.queryForObject(format("SELECT * FROM %s WHERE id = %d", tableName, id), ROW_MAPPER);
            return Optional.ofNullable(message);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Message> findByMessage(String messageText) {
        try {
            Message message = super.queryForObject(format("SELECT * FROM %s WHERE username = '%s';", tableName, messageText), ROW_MAPPER);
            return (Optional.ofNullable(message));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Message> findAll()  {
        return super.query(format("SELECT * FROM %s;", tableName), ROW_MAPPER);
    }

    @Override
    public void save(Message entity)  {
        super.update(format("INSERT INTO %s (sender, message, time) VALUES ('%s', '%s', '%s');", tableName, entity.getSender(), entity.getMessage(), entity.getTime()));
    }

    @Override
    public void update(Message entity) {
        super.update(format("UPDATE %s SET sender = '%s', message = '%s', time = '%s' WHERE id = %d;",
                tableName, entity.getSender(), entity.getMessage(), entity.getTime(), entity.getId()));
    }

    @Override
    public void delete(Long id) {
        super.update(format("DELETE FROM %s WHERE id = %d;", tableName, id));
    }
}

