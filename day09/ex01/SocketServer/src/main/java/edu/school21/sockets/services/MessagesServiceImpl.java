package edu.school21.sockets.services;

import edu.school21.sockets.models.Message;
import edu.school21.sockets.repositories.MessagesRepository;
import edu.school21.sockets.repositories.MessagesRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.time.LocalDateTime;

@Service
public class MessagesServiceImpl implements MessagesService {

    private MessagesRepository messagesRepository;

    public MessagesServiceImpl() {}

    @Autowired
    @Qualifier("messagesRepository")
    public void setMessagesRepository(MessagesRepositoryImpl messagesRepository) {
        this.messagesRepository = messagesRepository;
    }

    @Override
    public void saveMessage(String sender, String text, LocalDateTime time) throws SQLException {
        messagesRepository.save(new Message(0L, sender, text, time));
    }
}
