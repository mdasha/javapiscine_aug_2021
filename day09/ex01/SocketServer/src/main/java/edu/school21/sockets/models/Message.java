package edu.school21.sockets.models;

import java.time.LocalDateTime;
import java.util.Objects;

public class Message {
    private Long id;
    private String sender;
    private String message;
    private LocalDateTime time;

    public Message(Long id, String sender, String message, LocalDateTime time) {
        this.id = id;
        this.sender = sender;
        this.message = message;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message message1 = (Message) o;
        return Objects.equals(id, message1.id) && Objects.equals(sender, message1.sender) && Objects.equals(message, message1.message) && Objects.equals(time, message1.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sender, message, time);
    }

    @Override
    public String toString() {
        return "Message: {" +
                "id=" + id +
                ", sender='" + sender + '\'' +
                ", message='" + message + '\'' +
                ", time=" + time +
                '}';
    }
}