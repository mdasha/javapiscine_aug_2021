package edu.school21.sockets.services;

import java.sql.SQLException;
import java.time.LocalDateTime;

public interface MessagesService {
    void saveMessage(String sender, String text, LocalDateTime time) throws SQLException;
}
