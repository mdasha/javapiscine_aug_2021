create table if not exists chat
(
    id       bigint generated always as identity,
    username varchar(30) unique not null,
    password varchar(200) not null
);

alter table chat
    owner to postgres;

create table if not exists message
(
    id       bigint generated always as identity,
    message varchar(30) not null,
    sender varchar(200) not null,
    time varchar(200) not null
    );

alter table message
    owner to postgres;

