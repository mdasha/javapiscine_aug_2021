#!/bin/bash
mkdir target && mkdir target/edu.school21.printer
javac -d target/edu.school21.printer src/java/edu.school21.printer/app/*.java src/java/edu.school21.printer/logic/BmpImgIntoCharArr.java
java -cp target/edu.school21.printer Program --white=. --black=0 ../../ex01/ImagesToChar/src/resources/it.bmp
rm -rf target