public class Program {

    public static void main(String[] args) throws IllegalNumberOfArgumentsError {

        try {
            if (args.length != 3) {
                throw new IllegalNumberOfArgumentsError();
            }
            String [] whiteChar = args[0].split("=");
            String [] blackChar = args[1].split("=");
            String imgPath = args[2];

            BmpImgIntoCharArr bmpIntoCharArray = new BmpImgIntoCharArr(whiteChar[1], blackChar[1], imgPath);
            bmpIntoCharArray.printCharImage();
        } catch(IllegalNumberOfArgumentsError e) {
            System.out.println("Error: " + e.getMessage());
        }
        System.exit(0);
    }
}