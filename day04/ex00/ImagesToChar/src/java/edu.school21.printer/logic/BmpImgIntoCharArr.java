import javax.imageio.ImageIO;
import java.io.File;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class BmpImgIntoCharArr  {
    private final String blackChar;
    private final String whiteChar;
    private final String imgPath;

    BmpImgIntoCharArr(String whiteChar, String blackChar,  String imgPath) {
        this.blackChar = blackChar;
        this.whiteChar = whiteChar;
        this.imgPath = imgPath;
    }

    void printCharImage() {
        try {
            File file = new File(imgPath);
            BufferedImage image = ImageIO.read(file);

            for (int y = 0; y < image.getHeight(); y++) {
                for (int x = 0; x < image.getWidth(); x++) {
                    Color color = new Color(image.getRGB(x, y));
                    if (color.getRed() == 255 && color.getGreen() == 255 && color.getBlue() == 255) {
                        System.out.print(whiteChar);
                    }
                    else if (color.getRed() == 0 && color.getGreen() == 0 && color.getBlue() == 0)  {
                        System.out.print(blackChar);
                    }
                    else {
                        System.out.print("/");
                    }
                }
                System.out.println();
            }
        }
        catch (IOException e) {
            System.out.println("Файл не найден или не удалось сохранить");
        }
    }
}
