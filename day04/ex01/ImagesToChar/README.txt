#!/bin/bash
mkdir target && mkdir target/resources
cp -a src/resources target
javac -d target src/java/edu.school21.printer/app/*.java src/java/edu.school21.printer/logic/BmpImgIntoCharArr.java
jar cvfm target/images-to-chars-printer.jar src/manifest.txt -C target .
java -jar target/images-to-chars-printer.jar