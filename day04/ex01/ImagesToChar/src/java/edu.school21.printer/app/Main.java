package edu.school21.printer.app;

public class Main {

    public static void main(String[] args) throws IllegalNumberOfArgumentsError {

        try {
            if (args.length != 0) {
                throw new IllegalNumberOfArgumentsError();
            }
            String whiteChar = ".";
            String blackChar = "0";
            String imgPath = "target/resources/it.bmp";

            BmpImgIntoCharArr bmpIntoCharArray = new BmpImgIntoCharArr(whiteChar, blackChar, imgPath);
            bmpIntoCharArray.printCharImage();
        } catch(IllegalNumberOfArgumentsError e) {
            System.out.println("Error: " + e.getMessage());
        }
        System.exit(0);
    }
}