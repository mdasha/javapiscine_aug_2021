package edu.school21.printer.app;

public class IllegalNumberOfArgumentsError extends Exception {
    public IllegalNumberOfArgumentsError() {
        super("Аргументы не нужны");
    }
}