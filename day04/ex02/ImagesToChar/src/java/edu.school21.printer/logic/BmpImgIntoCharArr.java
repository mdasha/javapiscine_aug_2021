package edu.school21.printer.logic;
import javax.imageio.ImageIO;
import java.io.File;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;

public class BmpImgIntoCharArr  {
    private final String blackChar;
    private final String whiteChar;
    private final String imgPath;

    BmpImgIntoCharArr(String whiteChar, String blackChar,  String imgPath) {
        this.blackChar = blackChar;
        this.whiteChar = whiteChar;
        this.imgPath = imgPath;
    }

    void printCharImage() {
        try {
            File file = new File(imgPath);
            BufferedImage image = ImageIO.read(file);

            Ansi.BColor.valueOf(whiteChar);
            Ansi.BColor.valueOf(blackChar);

            for (int y = 0; y < image.getHeight(); y++) {
                for (int x = 0; x < image.getWidth(); x++) {
                    ColoredPrinter cp = new ColoredPrinter();
                    if (image.getRGB(x, y) == -1) {
                        cp.print(" ", Ansi.Attribute.NONE, Ansi.FColor.NONE, Ansi.BColor.valueOf(whiteChar));
                    }
                    else   {
                        cp.print(" ", Ansi.Attribute.NONE, Ansi.FColor.NONE, Ansi.BColor.valueOf(blackChar));
                    }

                }
                System.out.println();
            }
        }
        catch (IOException e) {
            System.out.println("Файл не найден или не удалось сохранить");
        }
    }
}
