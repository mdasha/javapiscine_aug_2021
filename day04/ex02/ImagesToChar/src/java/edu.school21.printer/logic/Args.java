package edu.school21.printer.logic;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.io.IOException;

@Parameters(separators = "=")

public class Args {
    @Parameter(names={"--white"})
    private String arg1;
    @Parameter(names={"--black"})
    private String arg2;

    public void run() throws IOException {
        try {
            String imgPath = "target/resources/it.bmp";
            BmpImgIntoCharArr bmpIntoCharArray = new BmpImgIntoCharArr(arg1, arg2, imgPath);
            bmpIntoCharArray.printCharImage();
        }
        catch (Exception e) {
            System.out.println("Error: file not valid or invalid parameter");
        }

    }
}