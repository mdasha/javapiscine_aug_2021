package edu.school21.printer.app;
import java.io.*;
import com.beust.jcommander.JCommander;
import edu.school21.printer.logic.Args;

public class Main {

    public static void main(String[] args) throws IllegalNumberOfArgumentsError, IOException {

        try {
            if (args.length != 2) {
                throw new IllegalNumberOfArgumentsError();
            }
            try {
                Args arguments = new Args();
                JCommander.newBuilder()
                        .addObject(arguments)
                        .build()
                        .parse(args);
                arguments.run();
            }
            catch (Exception e) {
                System.out.println("Invalid color");
            }
        } catch(IllegalNumberOfArgumentsError e) {
            System.out.println("Error: " + e.getMessage());
        }
        System.exit(0);
    }
}