package edu.school21.printer.app;

public class IllegalNumberOfArgumentsError extends Exception {
    public IllegalNumberOfArgumentsError() {
        super("Пример: --white=. --black=0 it.bmp");
    }
}