package edu.school21.numbers;

public class IllegalNumberException extends Throwable {
   public IllegalNumberException() {
      super("Введите натуральное положительное число большее 1");
   }
}
