package edu.school21.repositories;

import edu.school21.exception.NotSavedSubEntityException;
import edu.school21.models.Product;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductsReposutoryJdbcImpl implements ProductsRepository {

    private final DataSource dataSource;

    public ProductsReposutoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Product> findAll() throws NotSavedSubEntityException {
        List<Product> products = new ArrayList<>();
        Long i = 0L;
        Optional<Product> productOptional;
        while ((productOptional = findById(i)).isPresent()) {
            products.add(productOptional.get());
            i++;
        }
        if (products.isEmpty()) {
            return null;
        }
        return products;
    }

    public Optional<Product> findById(Long id) throws NotSavedSubEntityException {
        Product product;
        Optional<Product> productOptional = Optional.empty();
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select * from product.productTable " +
                    "where identifier = ?");
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                product = new Product(
                        rs.getLong("identifier"),
                        rs.getString("name"),
                        rs.getLong("price")
                );
                productOptional = Optional.of(product);
            }
            if (!rs.next()) {
                rs.close();
                ps.close();
                conn.close();
            }

        } catch (SQLException e) {
            throw new NotSavedSubEntityException(e);
        }
        return productOptional;
    }

    public void update(Product product) throws NotSavedSubEntityException {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "update product.productTable " +
                            "SET name = ?," +
                            "price = ? where identifier = ?"
            );

            ps.setString(1, product.getName());
            ps.setLong(2, product.getPrice());
            ps.setLong(3, product.getId());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new NotSavedSubEntityException(e);
        }
    }

    public void save(Product product) throws NotSavedSubEntityException {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "insert into product.productTable(name, price) " +
                            "VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, product.getName());
            ps.setLong(2, product.getPrice());

            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                long productId = rs.getLong(1);
                product.setId(productId);
            } else {
                conn.close();
                ps.close();
                rs.close();
            }
        } catch (SQLException e) {
            throw new NotSavedSubEntityException(e);
        }
    }

    public void delete (Long id) throws NotSavedSubEntityException {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "delete from product.productTable where identifier = ?");
            ps.setLong(1, id);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new NotSavedSubEntityException(e);
        }
    }
}
