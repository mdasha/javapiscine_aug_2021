package edu.school21.repositories;

import edu.school21.exception.NotSavedSubEntityException;
import edu.school21.models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductsRepository {
    List<Product> findAll() throws NotSavedSubEntityException;
    Optional<Product> findById(Long id) throws NotSavedSubEntityException;
    void update(Product product) throws NotSavedSubEntityException;
    void save(Product product) throws NotSavedSubEntityException;
    void delete (Long id) throws NotSavedSubEntityException;
}
