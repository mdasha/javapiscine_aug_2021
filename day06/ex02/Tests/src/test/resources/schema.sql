DROP SCHEMA IF EXISTS product CASCADE;
CREATE SCHEMA product;

create table if not exists product.productTable
(
    identifier INT identity PRIMARY KEY,
    name  VARCHAR(50) NOT NULL,
    price INT NOT NULL
);
