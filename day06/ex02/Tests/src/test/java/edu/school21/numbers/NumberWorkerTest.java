package edu.school21.numbers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.ValueSource;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class NumberWorkerTest {

    NumberWorker numberWorker;

    @BeforeEach
    void prepare() {
        System.out.println("Before each: " + this);
        numberWorker = new NumberWorker();
    }

    @ParameterizedTest
    @ValueSource(ints = {3,5,13,421,599})
    void isPrimeForPrimes(int number) throws IllegalNumberException {
        System.out.println("Test 1: " + this);
        Assertions.assertTrue(numberWorker.isPrime(number));
    }

    @ParameterizedTest
    @ValueSource(ints = {28,4,9,8,256})
    void isPrimeForNotPrimes(int number) throws IllegalNumberException {
        System.out.println("Test 2: " + this);
        Assertions.assertFalse(numberWorker.isPrime(number));
    }

    @ParameterizedTest
    @ValueSource(ints = {0,1,-1})
    void isPrimeForIncorrectNumbers(int number) throws IllegalNumberException {
        System.out.println("Test 3: " + this);
        Assertions.assertThrows(IllegalNumberException.class, () -> {
            numberWorker.isPrime(number);
        });
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/data.csv")
    void csvFileSourceTest(int input, int expected) {
        System.out.println("Test 4: " + this);
        Assertions.assertEquals(expected, numberWorker.digitSum(input));
    }

}
