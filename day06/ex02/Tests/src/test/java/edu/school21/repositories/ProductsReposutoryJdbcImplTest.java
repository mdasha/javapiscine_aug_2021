package edu.school21.repositories;

import edu.school21.exception.NotSavedSubEntityException;
import edu.school21.models.Product;
import org.junit.jupiter.api.*;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ProductsReposutoryJdbcImplTest {

    private static EmbeddedDatabase eBuildedDB;
    static final List<Product> EXPECTED_FIND_ALL_PRODUCTS = new ArrayList<>();
    final Product EXPECTED_FIND_BY_ID_PRODUCT = new Product(4L, "Spoons", 800);
    final Product EXPECTED_UPDATED_PRODUCT = new Product(2L, "Apple mouse", 7000);
    final Product EXPECTED_SAVED_PRODUCT = new Product(5L, "Молоко", 100);
    private ProductsReposutoryJdbcImpl productsReposutoryJdbcImpl;

    @BeforeAll
    public static void initFields() {
        EXPECTED_FIND_ALL_PRODUCTS.add(new Product(0L, "table Rosalia", 11220));
        EXPECTED_FIND_ALL_PRODUCTS.add(new Product(1L, "Apple IPhone11", 65223));
        EXPECTED_FIND_ALL_PRODUCTS.add(new Product(2L, "PC mouse", 3000));
        EXPECTED_FIND_ALL_PRODUCTS.add(new Product(3L, "Webcam A4 Tech", 2500));
        EXPECTED_FIND_ALL_PRODUCTS.add(new Product(4L, "Spoons", 800));
    }

    @BeforeEach
    public void init() {
        EmbeddedDatabaseBuilder eBuilder = new EmbeddedDatabaseBuilder();

        eBuildedDB = eBuilder
                .setType(EmbeddedDatabaseType.HSQL)
                .ignoreFailedDrops(true)
                .addScript("schema.sql")
                .addScript("data.sql")
                .build();

        productsReposutoryJdbcImpl = new ProductsReposutoryJdbcImpl(eBuildedDB);
    }

    @AfterEach
    public void closeDb() {
        try {
            eBuildedDB.getConnection().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getConnection() throws SQLException {
        Assertions.assertNotNull(eBuildedDB.getConnection());
    }

    @Test
    public void fidAllShouldReturnAllTest() throws NotSavedSubEntityException {
        Assertions.assertEquals(EXPECTED_FIND_ALL_PRODUCTS, productsReposutoryJdbcImpl.findAll());
    }

    @Test
    public void findByIdTest() throws NotSavedSubEntityException {
        Assertions.assertEquals(EXPECTED_FIND_BY_ID_PRODUCT, productsReposutoryJdbcImpl.findById(4L).get());
    }

    @Test
    public void saveTest() throws NotSavedSubEntityException {
        productsReposutoryJdbcImpl.save(EXPECTED_SAVED_PRODUCT);
        Assertions.assertEquals(EXPECTED_SAVED_PRODUCT, productsReposutoryJdbcImpl.findById(EXPECTED_SAVED_PRODUCT.getId()).get());
    }

    @Test
    public void UpdateTest() throws NotSavedSubEntityException {
        productsReposutoryJdbcImpl.update(EXPECTED_UPDATED_PRODUCT);
        Assertions.assertEquals(EXPECTED_UPDATED_PRODUCT, productsReposutoryJdbcImpl.findById(EXPECTED_UPDATED_PRODUCT.getId()).get());
    }

    @Test
    public void DeleteTest() throws NotSavedSubEntityException {
       productsReposutoryJdbcImpl.delete(4L);
       Assertions.assertFalse(productsReposutoryJdbcImpl.findById(EXPECTED_FIND_ALL_PRODUCTS.get(4).getId()).isPresent());
    }
}
