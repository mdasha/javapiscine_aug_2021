package edu.school21.numbers;

public class NumberWorker {
    public static void main(String[] args) {
        System.exit(0);
    }

    public boolean isPrime(int number) throws IllegalNumberException {
        if (number < 2) {
            throw new IllegalNumberException();
        }
        int divider = 2;

        while (divider * divider < number) {
            if (number % divider == 0) {
                break;
            }
            divider++;
        }
        if (number == 2) {
            return (true);
        } else {
            return (!(number % divider == 0));
        }
    }

    public int digitSum(int number) {
        int result = 0;

        while (number >= 10) {
            result += number % 10;
            number /= 10;
        }
        result += number % 10;
        return (result);
    }
}
