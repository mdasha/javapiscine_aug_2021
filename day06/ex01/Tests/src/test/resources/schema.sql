create table product
(
    identifier INT identity PRIMARY KEY,
    name  VARCHAR(50) NOT NULL,
    price INT NOT NULL
);
