package edu.school21.repositories;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import java.sql.SQLException;

public class EmbeddedDataSourceTest {

    EmbeddedDatabase eBuildedDB;

    @BeforeEach
    public void init() {
        EmbeddedDatabaseBuilder eBuilder = new EmbeddedDatabaseBuilder();

        eBuildedDB = eBuilder
                .setType(EmbeddedDatabaseType.HSQL)
                .addScript("/schema.sql")
                .addScript("/data.sql")
                .build();
    }

    @Test
    public void getConnection() throws SQLException {
        Assertions.assertNotNull(eBuildedDB.getConnection());
    }

}
