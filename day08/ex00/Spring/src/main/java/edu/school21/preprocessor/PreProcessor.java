package edu.school21.preprocessor;

public interface PreProcessor {
    void setMessage(String message);
    String getMessage();
}
