package edu.school21.preprocessor;

public class PreProcessorToLowerImpl implements PreProcessor {
    String message;

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message.toLowerCase();
    }
}
