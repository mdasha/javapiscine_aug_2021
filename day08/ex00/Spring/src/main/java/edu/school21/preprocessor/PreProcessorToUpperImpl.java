package edu.school21.preprocessor;

public class PreProcessorToUpperImpl implements PreProcessor {
    String message;

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message.toUpperCase();
    }
}
