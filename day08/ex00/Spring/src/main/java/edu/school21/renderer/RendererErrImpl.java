package edu.school21.renderer;

import edu.school21.preprocessor.PreProcessor;
import java.io.PrintStream;

public class RendererErrImpl implements Renderer {

    private PreProcessor preProcessor;
    private PrintStream stream;
    private String message;

    public RendererErrImpl(PreProcessor preProcessor) {
        this.preProcessor = preProcessor;
        setStream(System.err);
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        preProcessor.setMessage(message);
        this.message = preProcessor.getMessage();
    }

    public PreProcessor getPreProcessor() {
        return preProcessor;
    }

    public void setPreProcessor(PreProcessor preProcessor) {
        this.preProcessor = preProcessor;
    }

    @Override
    public PrintStream getStream() {
        return stream;
    }

    @Override
    public void setStream(PrintStream stream) {
        this.stream = stream;
    }
}



