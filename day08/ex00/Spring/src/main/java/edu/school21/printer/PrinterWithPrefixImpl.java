package edu.school21.printer;

import edu.school21.renderer.Renderer;
import java.io.PrintStream;

public class PrinterWithPrefixImpl implements Printer {

    private String message;
    private String prefix;
    private final Renderer renderer;

    public PrinterWithPrefixImpl(Renderer renderer) {
        this.renderer = renderer;
        this.prefix = "";
        this.message = "";
    }

    public Renderer getRenderer() {
        return renderer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public void print(String message) {
        PrintStream stream = this.getRenderer().getStream();
        if (!this.getPrefix().equals("")) {
            String msg = this.getPrefix() + " " + message;
            renderer.setMessage(msg);
            msg = renderer.getMessage();
            stream.println(msg);
        }
        else {
            renderer.setMessage(message);
            message = renderer.getMessage();
            stream.println(message);
        }
    }
}
