package edu.school21.printer;

import edu.school21.renderer.Renderer;
import java.io.PrintStream;
import java.time.LocalDateTime;

public class PrinterWithDateTimeImpl implements Printer {

        private String message;
        private final Renderer renderer;

    public PrinterWithDateTimeImpl(Renderer renderer) {
        this.renderer = renderer;
        this.message = "";
    }

    public Renderer getRenderer() {
        return renderer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void print(String message) {
        PrintStream stream = this.getRenderer().getStream();
        renderer.setMessage(LocalDateTime.now() + ":\n" + message);
        message = renderer.getMessage();
        stream.println(message);
    }
}
