package edu.school21.renderer;

import java.io.PrintStream;

public interface Renderer {
    void setStream(PrintStream stream);
    PrintStream getStream();
    void setMessage(String message);
    String getMessage();
}
