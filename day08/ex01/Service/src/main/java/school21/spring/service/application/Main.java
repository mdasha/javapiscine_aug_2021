package school21.spring.service.application;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import school21.spring.service.exception.NotSavedSubEntityException;
import school21.spring.service.models.User;
import school21.spring.service.repositories.UsersRepository;

public class Main {
    public static void main(String[] args) throws NotSavedSubEntityException {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        UsersRepository usersRepository = context.getBean("usersRepositoryJdbcHikari", UsersRepository.class);

        System.out.println("USER REPOSITORY JDBC");
        System.out.println("ALL USERS: ");
        System.out.println(usersRepository.findAll());
        System.out.println("");
        System.out.println("Find USER BY ID = 2");
        System.out.println(usersRepository.findById(2L));
        System.out.println("");

        System.out.println("SAVE NEW USER");
        User user = new User(null, "snaomi@student.21-school.ru");
        usersRepository.save(user);
        System.out.println(usersRepository.findByEmail("snaomi@student.21-school.ru").get());
        System.out.println("");

        System.out.println("UPDATE USER");
        Long idForUpdateDelete = usersRepository.findByEmail("snaomi@student.21-school.ru").get().getIdentifier();
        System.out.println("ID FOR UPDATE: " + idForUpdateDelete);
        User updateUser = new User(idForUpdateDelete, "drina@student.21-school.ru");
        System.out.println("USER BEFORE UPDATE: " + usersRepository.findById(idForUpdateDelete));
        usersRepository.update(updateUser);
        System.out.println("USER AFTER UPDATE: " + usersRepository.findById(idForUpdateDelete));
        System.out.println("");

        System.out.println("ALL USERS BEFORE DELETE: ");
        System.out.println(usersRepository.findAll());
        System.out.println("");

        System.out.println("DELETE USER");
        System.out.println("ID FOR DELETE: " + idForUpdateDelete);
        System.out.println("USER BEFORE DELETE: " + usersRepository.findById(idForUpdateDelete));
        usersRepository.delete(idForUpdateDelete);
        System.out.println("");
        System.out.println("USERS AFTER DELETE:\n");
        System.out.println(usersRepository.findAll());

        System.out.println("");
        System.out.println("USER REPOSITORY JDBC TEMPLATE");
        System.out.println("ALL USERS");
        usersRepository = context.getBean("usersRepositoryJdbcTemplateHikari", UsersRepository.class);
        System.out.println(usersRepository.findAll ());

        System.out.println("");
        System.out.println("Find USER BY ID = 2");
        System.out.println(usersRepository.findById(2L));
        System.out.println("");

        System.out.println("SAVE NEW USER");
        User user2 = new User(null, "snaomi@student.21-school.ru");
        usersRepository.save(user2);
        System.out.println(usersRepository.findByEmail("snaomi@student.21-school.ru").get());
        System.out.println("");

        System.out.println("UPDATE USER");
        Long idForUpdateDelete2 = usersRepository.findByEmail("snaomi@student.21-school.ru").get().getIdentifier();
        System.out.println("ID FOR UPDATE: " + idForUpdateDelete2);
        User updateUser2 = new User(idForUpdateDelete2, "drina@student.21-school.ru");
        System.out.println("USER BEFORE UPDATE: " + usersRepository.findById(idForUpdateDelete2));
        usersRepository.update(updateUser2);
        System.out.println("USER AFTER UPDATE: " + usersRepository.findById(idForUpdateDelete2));
        System.out.println("");

        System.out.println("ALL USERS BEFORE DELETE: ");
        System.out.println(usersRepository.findAll());
        System.out.println("");

        System.out.println("DELETE USER");
        System.out.println("ID FOR DELETE: " + idForUpdateDelete2);
        System.out.println("USER BEFORE DELETE: " + usersRepository.findById(idForUpdateDelete2));
        usersRepository.delete(idForUpdateDelete2);
        System.out.println("");
        System.out.println("USERS AFTER DELETE:\n");
        System.out.println(usersRepository.findAll());

        System.exit(0);
    }
}
