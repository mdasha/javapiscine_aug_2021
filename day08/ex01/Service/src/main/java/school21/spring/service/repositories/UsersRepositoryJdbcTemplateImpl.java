package school21.spring.service.repositories;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import school21.spring.service.exception.NotSavedSubEntityException;
import school21.spring.service.models.User;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryJdbcTemplateImpl implements UsersRepository {

    private final DataSource dataSource;

    public UsersRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public User findById(Long id)  {

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        String sql = "select * from users.userTable where identifier = ?";
        return jdbcTemplate.queryForObject(sql, (rs, rowNum) ->
                new User(
                        rs.getLong("identifier"),
                        rs.getString("email")
                ), id);
    }

    public Optional<User> findByEmail(String email)  {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        String sql = "select * from users.userTable where email = ?";
        List<User> users = jdbcTemplate.query(sql, (rs, rowNum) ->
                new User(
                        rs.getLong("identifier"),
                        rs.getString("email")
                ), email);
        return Optional.of(users.get(users.size() - 1));
    }

    public List<User> findAll() throws NotSavedSubEntityException {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        String sql = "select * from users.userTable";
        return jdbcTemplate.query(
                sql,
                (rs, rowNum) ->
                        new User(
                                rs.getLong("identifier"),
                                rs.getString("email")
                        )
        );
    }

    public void save(User user)  {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        String insertSql = "insert into users.userTable(email) VALUES (?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(insertSql, new String[] { "identifier" });
            ps.setString(1, user.getEmail());
            return ps;
        }, keyHolder);
    }

    public void update(User user)  {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.update(
                "update users.userTable set email = ? where identifier = ?",
                user.getEmail(), user.getIdentifier());
    }

    public void delete(Long id) throws NotSavedSubEntityException {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.update(
                "delete from users.userTable where identifier = ?",
                Long.valueOf(id));
    }
}
