package school21.spring.service.repositories;

import school21.spring.service.exception.NotSavedSubEntityException;
import school21.spring.service.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryJdbcImpl implements UsersRepository {

    private final DataSource dataSource;

    public UsersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public User findById(Long id) throws NotSavedSubEntityException {
        User user;
        Optional<User> userOptional = Optional.empty();
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select * from users.userTable " +
                    "where identifier = ?");
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = new User(
                        rs.getLong("identifier"),
                        rs.getString("email")
                );
                userOptional = Optional.of(user);
            }
            if (!rs.next()) {
                rs.close();
                ps.close();
                conn.close();
            }

        } catch (SQLException e) {
            throw new NotSavedSubEntityException(e);
        }
        return userOptional.get();
    }

    public Optional<User> findByEmail(String email) throws NotSavedSubEntityException {
        User user;
        Optional<User> userOptional = Optional.empty();
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select * from users.userTable " +
                    "where email = ?");
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = new User(
                        rs.getLong("identifier"),
                        rs.getString("email")
                );
                userOptional = Optional.of(user);
            }
            if (!rs.next()) {
                rs.close();
                ps.close();
                conn.close();
            }

        } catch (SQLException e) {
            throw new NotSavedSubEntityException(e);
        }
        return userOptional;
    }

    public List<User> findAll() throws NotSavedSubEntityException {
        List<User> users = new ArrayList<>();

        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select * from users.userTable");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                users.add(new User(rs.getLong("identifier"), rs.getString("email")));
            }
            if (users.isEmpty()) {
                return null;
            }
        } catch (SQLException e) {
            throw new NotSavedSubEntityException(e);
        }
        return users;
    }

    public void save(User user) throws NotSavedSubEntityException {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "insert into users.userTable(email) " +
                            "VALUES (?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getEmail());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                long productId = rs.getLong(1);
                user.setIdentifier(productId);
            } else {
                conn.close();
                ps.close();
                rs.close();
            }
        } catch (SQLException e) {
            throw new NotSavedSubEntityException(e);
        }
    }

    public void update(User user) throws NotSavedSubEntityException {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "update users.userTable " +
                            "SET email = ? where identifier = ?"
            );

            ps.setString(1, user.getEmail());
            ps.setLong(2, user.getIdentifier());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new NotSavedSubEntityException(e);
        }
    }

    public void delete(Long id) throws NotSavedSubEntityException {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "delete from users.userTable where identifier = ?");
            ps.setLong(1, id);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new NotSavedSubEntityException(e);
        }
    }
}
