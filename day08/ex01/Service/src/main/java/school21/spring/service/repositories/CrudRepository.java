package school21.spring.service.repositories;

import school21.spring.service.exception.NotSavedSubEntityException;

import java.util.List;

public interface CrudRepository<T> {
    T findById(Long id) throws NotSavedSubEntityException;
    List<T> findAll() throws NotSavedSubEntityException;
    void save(T entity) throws NotSavedSubEntityException;
    void update(T entity) throws NotSavedSubEntityException;
    void delete(Long id) throws NotSavedSubEntityException;
}
