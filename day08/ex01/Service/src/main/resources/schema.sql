DROP SCHEMA IF EXISTS users CASCADE;
CREATE SCHEMA users;

create table if not exists users.userTable
(
    identifier SERIAL PRIMARY KEY,
    email  VARCHAR(50) NOT NULL
);