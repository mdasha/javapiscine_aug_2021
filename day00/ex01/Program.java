package ex01;

import java.util.Scanner;

public class Program {

    public static void main(String[] args) {

        int number = requestNumber();

        if (number <= 1) {
            System.err.println("illegalArgument");
            System.exit(-1);
        }

        printPrimeSteps(number);
        System.exit(0);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    static void printPrimeSteps(int number) {
        int steps = 1;
        int divider = 2;

        while ( divider * divider < number ) {
            if ( number % divider == 0 ) {
                break;
            }
            steps++;
            divider++;
        }

        if (number == 2) {
            System.out.println("true 1");
        }
        else {
            System.out.println(!(number % divider == 0) + " " + steps);
        }
    }
}