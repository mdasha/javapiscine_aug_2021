package ex03;

import java.util.Scanner;

public class Program {

    public static void main(String[] args) {

        String week;
        String initWeek;
        long grades = 0;
        long multiplier = 1;
        int counter = 0;

        while (true) {
            week = requestString();
            if (week.equals("42")) {
                break ;
            }
            else {
                counter++;
                initWeek = "Week " + counter;
            }
            if ((!(initWeek.equals(week)) ||  counter > 18) && counter != 0  ) {
                exitWithError();
            }
            long minGrade = getMinGrade();
            grades = minGrade * multiplier + grades;
            multiplier *= 10;
        }

        int countWeeks = 0;
        if (counter != 0) {
            countWeeks = countWeeks(grades);
        }
        printChar(countWeeks, grades);
        System.exit(0);
    }

    static int countWeeks(long grades) {
        long gradesTmp = grades;
        int countWeeks = 1;
        while (gradesTmp  > 10) {
            gradesTmp /= 10;
            countWeeks++;
        }
        return (countWeeks);
    }

    static void printChar(int countWeeks, long grades) {
        int i = 1;
        while (i <= countWeeks) {
            System.out.print("Week ");
            System.out.print(i);
            System.out.print(" ");
            int j = 0;

            while (j < grades % 10) {
                System.out.print("=");
                j++;
            }
            System.out.println(">");
            grades /= 10;
            i++;
        }
    }

    static long getMinGrade() {
        Scanner scan = new Scanner(System.in);
        int i = 1;
        long initGrade = scan.nextLong();
        if (initGrade > 9 || initGrade < 1) {
            exitWithError();
        }
        long minGrade = initGrade;

        while (i < 5) {
            initGrade = scan.nextLong();
            if (initGrade > 9 || initGrade < 1) {
                exitWithError();
            }
            if (initGrade < minGrade) {
                minGrade = initGrade;
            }
            i++;
        }
        return (minGrade);
    }

    static String requestString() {
        Scanner var0 = new Scanner(System.in);
        return var0.nextLine();
    }

    static void exitWithError() {
            System.err.println("illegalArgument");
            System.exit(-1);
    }
}