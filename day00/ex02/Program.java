package ex02;

import java.util.Scanner;

public class Program {

    public static void main(String[] args) {

        int number = 0;
        int countCoffeeRequest = 0;
        Scanner scan = new Scanner(System.in);

        while (number != 42) {
            number = scan.nextInt();

            if (number <= 1) {
                System.err.println("IllegalArgument");
                System.exit(-1);
            }

            int digitSum = countDigitSum(number);
            if (isPrime(digitSum)) {
                countCoffeeRequest++;
            }
        }
        System.out.println("Count of coffee - request - " + countCoffeeRequest);
        scan.close();
        System.exit(0);
    }

    static int countDigitSum(int number) {
        int result = 0;

        while (number > 10) {
            result += number % 10;
            number /= 10;
        }
        result += number % 10;
        return (result);
    }

    static boolean isPrime(int number) {
        int divider = 2;

        while ( divider * divider < number ) {
            if ( number % divider == 0 ) {
                break;
            }
            divider++;
        }
        if (number == 2) {
            return (true);
        }
        else {
            return(!(number % divider == 0));
        }
    }
}