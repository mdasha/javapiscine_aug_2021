create table users
(
    user_id SERIAL PRIMARY KEY,
    login  VARCHAR(50) NOT NULL,
    password VARCHAR(100) NOT NULL
);

create table chatroom
(
    chatroom_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    owner INT REFERENCES users(user_id)
);

create table message
(
    message_id SERIAL PRIMARY KEY,
    author INT REFERENCES users(user_id) NOT NULL,
    room INT REFERENCES chatroom(chatroom_id) NOT NULL,
    text VARCHAR(200) NOT NULL,
    time timestamp NOT NULL
);





