package edu.school21.chat.app;

import edu.school21.chat.models.Message;
import edu.school21.chat.repositories.MessagesRepository;
import edu.school21.chat.repositories.MessagesRepositoryJdbcImpl;

import java.time.LocalDateTime;
import java.util.Optional;

public class Program {
    public static void main(String[] args) {
        MessagesRepository messagesRepository = new MessagesRepositoryJdbcImpl();
        Optional<Message> messageOptional = messagesRepository.findById(4L);
        Optional<Message> messageOptional2 = messagesRepository.findById(5L);
        if (messageOptional.isPresent()) {
            Message message = messageOptional.get();
            message.setText("GoodBye");
            message.setDate(null);
            messagesRepository.update(message);
        }
        if (messageOptional2.isPresent()) {
            Message message2 = messageOptional2.get();
            message2.setText("GoodBye2");
            message2.setDate(LocalDateTime.now());
            messagesRepository.update(message2);
        }
            System.exit(0);
    }
}
