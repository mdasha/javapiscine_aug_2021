package edu.school21.chat.models;

import java.util.List;
import java.util.Objects;

public class Chatroom {
    private Long id;
    private String name;
    private Long owner;
    private User user;
    private List<Message> chatroomMessages;

    public Chatroom(Long id, String name, User user, List<Message> chatroomMessages) {
        this.id = id;
        this.name = name;
        this.user = user;
        this.chatroomMessages = chatroomMessages;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Message> getChatroomMessages() {
        return chatroomMessages;
    }

    public void setChatroomMessages(List<Message> chatroomMessages) {
        this.chatroomMessages = chatroomMessages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Chatroom)) return false;
        Chatroom chatroom = (Chatroom) o;
        return id.equals(chatroom.id) && name.equals(chatroom.name) && owner.equals(chatroom.owner) && user.equals(chatroom.user) && Objects.equals(chatroomMessages, chatroom.chatroomMessages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, owner, user, chatroomMessages);
    }

    @Override
    public String toString() {
        return "Chatroom: {" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", owner=" + owner +
                ", chatroomMessages=" + chatroomMessages +
                '}';
    }
}