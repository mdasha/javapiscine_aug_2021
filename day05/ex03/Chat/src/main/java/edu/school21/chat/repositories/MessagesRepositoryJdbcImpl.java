package edu.school21.chat.repositories;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import edu.school21.chat.exception.NotSavedSubEntityException;
import edu.school21.chat.models.Chatroom;
import edu.school21.chat.models.Message;
import edu.school21.chat.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;

public class MessagesRepositoryJdbcImpl implements MessagesRepository {
    private final DataSource dataSource;

    public MessagesRepositoryJdbcImpl() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/postgres");
        config.setUsername("dasha");
        config.setPassword("1111");

        dataSource = new HikariDataSource(config);
    }

    public Optional<Message> findById(Long id) {
        Message msg;
        Optional<Message> message = Optional.empty();
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement("select * from message " +
                    "left join users on message.author = users.user_id " +
                    "left join chatroom on message.room = chatroom.chatroom_id " +
                    "where message_id = ?");
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                User newUser = new User(rs.getLong("user_id"),
                        rs.getString("login"),
                        rs.getString("password"),
                        null,
                        null);
                Chatroom chatRoom = new Chatroom(rs.getLong("chatroom_id"),
                        rs.getString("name"),
                        null,
                        null
                );
                msg = new Message(
                        rs.getLong("message_id"),
                        newUser,
                        chatRoom,
                        rs.getString("text"),
                        Timestamp.valueOf(rs.getString("time")).toLocalDateTime()
                );
                message = Optional.of(msg);
            }
        } catch (SQLException e) {
            throw new NotSavedSubEntityException(e);
        }
        return message;
    }

    public void save(Message message) {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                    "insert into message(author, room, text, time) " +
                            "VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            ps.setLong(1, message.getUser().getId());
            ps.setLong(2, message.getChatRoom().getId());
            ps.setString(3, message.getText());
            ps.setTimestamp(4, Timestamp.valueOf(message.getDate()));

            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                long msgId = rs.getLong(1);
                message.setId(msgId);
            }
        } catch (SQLException e) {
            throw new NotSavedSubEntityException(e);
        }
    }

    public void update(Message message) {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(
                "update message " +
                        "SET text = ?," +
                        "time = ?," +
                        "author = ?," +
                        "room = ? where message_id = ?"
            );
            PreparedStatement ps2 = conn.prepareStatement(
                    "select * from message where message_id = ?"
            );
            ps2.setLong(1, message.getId());
            ps.setString(1, message.getText());

            ResultSet rs2 = ps2.executeQuery();
            if (message.getDate() != null) {
                ps.setTimestamp(2, Timestamp.valueOf(message.getDate()));
            }
            else {
                if (rs2.next()) {
                    ps.setTimestamp(2, rs2.getTimestamp("time"));
                }
            }
            ps.setLong(3, message.getUser().getId());
            ps.setLong(4, message.getChatRoom().getId());
            ps.setLong(5, message.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new NotSavedSubEntityException(e);
        }
    }
}

