package edu.school21.chat.exception;

public class NotSavedSubEntityException extends RuntimeException {
    public NotSavedSubEntityException(Throwable cause) {
        super("Error: нет такого user_id или author_id\n" + cause);
    }
}
