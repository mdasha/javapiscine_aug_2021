package edu.school21.chat.models;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

public class Message {
    private Long id;
    private Long author;
    private Long room;
    private String text;
    private LocalDateTime date;
    private User user;
    private Chatroom chatRoom;

    public Message(Long id, Long author, Long room, String text, LocalDateTime date, User user, Chatroom chatRoom) {
        this.id = id;
        this.author = author;
        this.room = room;
        this.text = text;
        this.date = date;
        this.user = user;
        this.chatRoom = chatRoom;
    }
    public Message(Long id, User user, Chatroom chatRoom, String text, LocalDateTime date) {
        this.id = id;
        this.user = user;
        this.chatRoom = chatRoom;
        this.text = text;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAuthor() {
        return author;
    }

    public void setAuthor(Long author) {
        this.author = author;
    }

    public Long getRoom() {
        return room;
    }

    public void setRoom(Long room) {
        this.room = room;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Chatroom getChatRoom() {
        return chatRoom;
    }

    public void setChatRoom(Chatroom chatRoom) {
        this.chatRoom = chatRoom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message message = (Message) o;
        return id.equals(message.id) && author.equals(message.author) && room.equals(message.room) && text.equals(message.text) && date.equals(message.date) && user.equals(message.user) && chatRoom.equals(message.chatRoom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, author, room, text, date, user, chatRoom);
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Message: {" +
                "\nid=" + id +
                ",\n" + user.toString() +
                ",\n" + chatRoom.toString() +
                ",\ntext='" + text + '\'' +
                ",\ndate=" + date +
                '}';
    }
}