package edu.school21.chat.app;

import edu.school21.chat.exception.NotSavedSubEntityException;
import edu.school21.chat.models.Chatroom;
import edu.school21.chat.models.Message;
import edu.school21.chat.models.User;
import edu.school21.chat.repositories.MessagesRepository;
import edu.school21.chat.repositories.MessagesRepositoryJdbcImpl;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) throws SQLException {
        User creator = new User(4L, "user", "user", new ArrayList(), new ArrayList());
        User author = creator;
        Chatroom room = new Chatroom(8L, "room", creator, new ArrayList());
        Chatroom room2 = new Chatroom(2L, "room", creator, new ArrayList());
        Message message2 = new Message(null, author, room2, "Hello!", LocalDateTime.now());
        MessagesRepository messagesRepository2 = new MessagesRepositoryJdbcImpl();
        messagesRepository2.save(message2);
        System.out.println(message2.getId());
        System.out.println("");
        Message message = new Message(null, author, room, "Hello!", LocalDateTime.now());
        MessagesRepository messagesRepository = new MessagesRepositoryJdbcImpl();
        messagesRepository.save(message);
        System.out.println(message.getId());
        System.exit(0);
    }
}
