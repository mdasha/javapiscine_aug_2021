package edu.school21.chat.models;

import java.util.List;
import java.util.Objects;

public class Chatroom {
    private Long id;
    private String name;
    private Long owner;
    private List<Message> chatroomMessages;

    public Chatroom(Long id, String name, Long owner, List<Message> chatroomMessages) {
        this.id = id;
        this.name = name;
        this.owner = owner;
        this.chatroomMessages = chatroomMessages;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

    public List<Message> getChatroomMessageModels() {
        return chatroomMessages;
    }

    public void setChatroomMessageModels(List<Message> chatroomMessageModels) {
        this.chatroomMessages = chatroomMessageModels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Chatroom)) return false;
        Chatroom chatroom = (Chatroom) o;
        return id.equals(chatroom.id) && name.equals(chatroom.name) && owner.equals(chatroom.owner) && Objects.equals(chatroomMessages, chatroom.chatroomMessages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, owner, chatroomMessages);
    }

    @Override
    public String toString() {
        return "Chatroom: {" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", owner=" + owner +
                ", chatroomMessages=" + chatroomMessages +
                '}';
    }
}