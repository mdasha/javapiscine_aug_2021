package edu.school21.chat.repositories;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import edu.school21.chat.exception.CustomException;
import edu.school21.chat.models.Chatroom;
import edu.school21.chat.models.Message;
import edu.school21.chat.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Optional;

public class MessagesRepositoryJdbcImpl implements MessagesRepository {
    private final DataSource dataSource;

    public MessagesRepositoryJdbcImpl() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/postgres");
        config.setUsername("dasha");
        config.setPassword("1111");

        dataSource = new HikariDataSource(config);
    }

    public Optional<Message> findById(Long id) {
        Message msg;
        Optional<Message> message = Optional.empty();
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps  = conn.prepareStatement("select * from message " +
                    "left join users on message.author = users.user_id " +
                    "left join chatroom on message.room = chatroom.chatroom_id " +
                    "where message_id = ?");
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Timestamp myTimestamp = Timestamp.valueOf(rs.getString("time"));
                String formattedTimestamp = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(myTimestamp);
                User newUser = new User (rs.getLong("user_id"),
                        rs.getString("login"),
                        rs.getString("password"),
                        null,
                        null);
                Chatroom chatRoom= new Chatroom(rs.getLong("chatroom_id"),
                        rs.getString("name"),
                        null,
                        null
                        );
                msg =  new Message(rs.getLong("message_id"),
                        newUser, chatRoom,
                        rs.getString("text"),
                        formattedTimestamp
                        );
                message = Optional.of(msg);
            }
        } catch (SQLException e) {
           throw new CustomException(e);
        }
        return message;
    }
}
