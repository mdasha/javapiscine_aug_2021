package edu.school21.chat.models;
import java.util.List;
import java.util.Objects;

public class User {
    private Long id;
    private String login;
    private String password;
    private List<Chatroom> createdRooms;
    private List<Chatroom> userSocializedRooms;

    public User(Long id, String login, String password, List<Chatroom> createdRooms, List<Chatroom> userSocializedRooms) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.createdRooms = createdRooms;
        this.userSocializedRooms = userSocializedRooms;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Chatroom> getCreatedRooms() {
        return createdRooms;
    }

    public void setCreatedRooms(List<Chatroom> createdRooms) {
        this.createdRooms = createdRooms;
    }

    public List<Chatroom> getUserSocializedRooms() {
        return userSocializedRooms;
    }

    public void setUserSocializedRooms(List<Chatroom> userSocializedRooms) {
        this.userSocializedRooms = userSocializedRooms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return id.equals(user.id) && login.equals(user.login) && password.equals(user.password) && Objects.equals(createdRooms, user.createdRooms) && Objects.equals(userSocializedRooms, user.userSocializedRooms);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password, createdRooms, userSocializedRooms);
    }

    @Override
    public String toString() {
        return "User: {" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", createdRooms=" + createdRooms +
                ", userSocializedRooms=" + userSocializedRooms +
                '}';
    }
}
