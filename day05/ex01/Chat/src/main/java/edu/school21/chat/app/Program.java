package edu.school21.chat.app;

import edu.school21.chat.models.Message;
import edu.school21.chat.repositories.MessagesRepositoryJdbcImpl;

import java.util.Optional;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        System.out.println("Enter a message ID:");
        Scanner scan = new Scanner(System.in);
        long messageId = scan.nextLong();
        MessagesRepositoryJdbcImpl messagesRepositoryJdbc = new MessagesRepositoryJdbcImpl();
        Optional<Message> message = messagesRepositoryJdbc.findById(messageId);
        if (message.isPresent()) {
            System.out.println(message.get());
        }
        else {
            System.out.println("Error: there is no message with id = " + messageId);
        }
        System.exit(0);
    }
}
