INSERT INTO users (login, password)
values('hemelia','hemelia'); -- 1
INSERT INTO users (login, password)
values('snaomi','snaomi'); -- 2
INSERT INTO users (login, password)
values('geldiss','geldiss'); -- 3
INSERT INTO users (login, password)
values('pcharlot','pcharlot'); -- 4
INSERT INTO users (login, password)
values('qsymond','qsymond'); -- 5

INSERT INTO chatroom (name, owner)
values('Java Piscine', 3); -- 1
INSERT INTO chatroom (name, owner)
values('Web Server', 1); -- 2
INSERT INTO chatroom (name, owner)
values('Adm', 2); -- 3
INSERT INTO chatroom (name, owner)
values('Bocal', 4); -- 4
INSERT INTO chatroom (name, owner)
values('Random', 5); -- 5

INSERT INTO message (author, room, text, time)
values(1, 1, 'Кто сделал задание 00 в day05?', '2021.08.30 13:00'); -- 1
INSERT INTO message (author, room, text, time)
values(2, 2, 'Когда будем делать бонусы?', '2021.08.30 13:05'); -- 2
INSERT INTO message (author, room, text, time)
values(3, 1, 'Я пока что разбираюсь с таблицами БД?', '2021.08.30 13:06'); -- 3
INSERT INTO message (author, room, text, time)
values(4, 1, 'А я не понимаю, как скачать Postgres. Помогите, пожаалуйста.', '2021.08.30 13:07'); -- 4
INSERT INTO message (author, room, text, time)
values(5, 2, 'У меня отпуск до 1 сентября. Вот 2 сентября и начнем.', '2021.08.30 13:01'); -- 5