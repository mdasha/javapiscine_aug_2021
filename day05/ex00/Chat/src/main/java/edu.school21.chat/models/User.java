package edu.school21.chat.models;
import java.util.List;
import java.util.Objects;

public class User {
    private Long id;
    private String login;
    private String password;
    private List<edu.school21.chat.models.Chatroom> createdRooms;
    private List<edu.school21.chat.models.Chatroom> userSocializedRooms;

    User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<edu.school21.chat.models.Chatroom> getCreatedRooms() {
        return createdRooms;
    }

    public void setCreatedRooms(List<edu.school21.chat.models.Chatroom> createdRooms) {
        this.createdRooms = createdRooms;
    }

    public List<edu.school21.chat.models.Chatroom> getUserSocializedRooms() {
        return userSocializedRooms;
    }

    public void setUserSocializedRooms(List<edu.school21.chat.models.Chatroom> userSocializedRooms) {
        this.userSocializedRooms = userSocializedRooms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id) && login.equals(user.login) && password.equals(user.password) && Objects.equals(createdRooms, user.createdRooms) && Objects.equals(userSocializedRooms, user.userSocializedRooms);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password, createdRooms, userSocializedRooms);
    }

    @java.lang.Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
