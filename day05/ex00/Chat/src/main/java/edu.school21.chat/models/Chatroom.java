package edu.school21.chat.models;

import java.util.List;
import java.util.Objects;

public class Chatroom {
    private Long id;
    private String name;
    private Long owner;
    private List<edu.school21.chat.models.Message> chatroomMessageModels;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

    public List<edu.school21.chat.models.Message> getChatroomMessageModels() {
        return chatroomMessageModels;
    }

    public void setChatroomMessageModels(List<edu.school21.chat.models.Message> chatroomMessageModels) {
        this.chatroomMessageModels = chatroomMessageModels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Chatroom)) return false;
        Chatroom chatroom = (Chatroom) o;
        return id.equals(chatroom.id) && name.equals(chatroom.name) && owner.equals(chatroom.owner) && chatroomMessageModels.equals(chatroom.chatroomMessageModels);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, owner, chatroomMessageModels);
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Chatroom{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", owner=" + owner +
                '}';
    }
}