class RealMultithreading extends Thread    {
    private final int id;
    private final int firstIndex;
    private final int lastIndex;
    private final int [] intArr;
    private int sumOfThreads;

    RealMultithreading(int id, int firstIndex, int lastIndex, int [] intArr) {
        this.id = id;
        this.firstIndex = firstIndex;
        this.lastIndex = lastIndex;
        this.intArr = intArr;
        this.sumOfThreads = 0;
    }

    int getSumOfThreads() {
        return (this.sumOfThreads);
    }

    @Override
    public void run() {
        int i = firstIndex;
        int sum = 0;
        while (i <= lastIndex) {
            sum += this.intArr[i];
            i++;
        }
        this.sumOfThreads += sum;
        System.out.println("Thread " + this.id + ": from " + this.firstIndex + " to " + this.lastIndex + " sum is " + sum);
    }
}