public class Program    {

    public static void main(String[] args) throws  InterruptedException, IllegalNumberOfArguments {
        if (args.length != 2) {
            throw new IllegalNumberOfArguments();
        }
        try {
            String [] sizeArray = args[0].split("=");
            String [] countArray = args[1].split("=");
            String countString = countArray[1];
            String arraySizeString = sizeArray[1];
            int count = Integer.parseInt(countString.trim());
            if (count <= 0) {
                System.err.println("Error: count must be positive number.");
                System.exit(-1);
            }
            int arraySize = Integer.parseInt(arraySizeString.trim());
                if (arraySize <= 0) {
                    System.err.println("Error: array size must be positive number.");
                    System.exit(-1);
            }
            int [] randomArray = generateRandomArray(arraySize);
            int sum = calculateSum(randomArray);
            System.out.println("Sum: " + sum);

            int otherThreadsLength = arraySize / count;
            int sumOfThreads = 0;
            int i = 1;
            int j = 0;
            while (i <= count) {
                RealMultithreading rmt;
                if (i == count) {
                    rmt = new RealMultithreading(i, j , arraySize - 1, randomArray);
                }
                else
                {
                   rmt = new RealMultithreading(i, j , j + otherThreadsLength - 1, randomArray);
                    j += otherThreadsLength;
                }
                rmt.start();
                rmt.join();
                sumOfThreads += rmt.getSumOfThreads();
                i++;
            }
            System.out.println("Sum by threads: " + sumOfThreads);
        }
        catch (NumberFormatException nfe) {
            System.out.println("Number Format Exception: " + nfe.getMessage());
        }
        System.exit(0);
    }

    public static int calculateSum(int []  randomArray) {
        int i = 0;
        int sum = 0;
        while (i < randomArray.length) {
            sum += randomArray[i];
            i++;
        }
        return (sum);
    }

    public static int [] generateRandomArray(int arraySize) {
        int [] randomArray = new int[arraySize];
        for (int i = 0; i < arraySize; i++) {
            randomArray[i] = (int) Math.round((Math.random() * 2000) - 1000);
        }
        return (randomArray);
    }
}