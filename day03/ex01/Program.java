public class Program    {
    public static void main(String[] args) throws  InterruptedException, IllegalNumberOfArguments {

        if (args.length != 1) {
            throw new IllegalNumberOfArguments();
        }
        try {
            String [] countArray = args[0].split("=");
            String countString = countArray[1];
            int count = Integer.parseInt(countString.trim());
            if (count <= 0) {
                System.err.println("Error: count must be positive number.");
                System.exit(-1);
            }
            final ProducerConsumer pc = new ProducerConsumer();

            Thread egg = new Thread(new Runnable() {
                @Override
                public void run()
                {
                    try {
                        int i = 0;
                        while (i < count) {
                            pc.produce(i);
                            i++;
                        }
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });

            Thread hen = new Thread(new Runnable() {
                @Override
                public void run()
                {
                    try {
                        int i = 0;
                        while (i < count) {
                            pc.consume();
                            i++;
                        }
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });

            egg.start();
            hen.start();
            egg.join();
            hen.join();
            int i = 0;
            while (i < count) {
                System.out.println("Human");
                i++;
            }
        }
        catch (NumberFormatException nfe) {
            System.out.println("Number Format Exception: " + nfe.getMessage());
        }
        System.exit(0);
    }
}