import java.util.LinkedList;
import java.util.List;

class ProducerConsumer {
    private final List<Integer> list = new LinkedList<>();
    int capacity = 1;

    public void produce(int i) throws InterruptedException {

        synchronized (this) {
            while (list.size() == capacity) {
                wait();
            }
            System.out.println("Egg");
            list.add(i++);
            notify();
            Thread.sleep(100);
        }
    }

    public void consume() throws InterruptedException {
            synchronized (this) {
                while (list.size() == 0) {
                    wait();
                }
                list.remove(0);
                System.out.println("Hen");
                notify();
                Thread.sleep(100);
            }
        }
}