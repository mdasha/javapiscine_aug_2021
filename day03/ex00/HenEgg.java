class HenEgg extends Thread    {
    private String message;
    private int count;

    HenEgg(String message, int count) {
        this.message = message;
        this.count = count;
    }

    @Override
    public void run() {
        for (int i = 0; i < this.count; i++) {
            System.out.println(this.message);
            try {
                sleep(100);
            } catch (Exception e) {}
        }
    }
}