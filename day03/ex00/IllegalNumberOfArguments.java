public class IllegalNumberOfArguments extends Exception {
    public IllegalNumberOfArguments() {
        super("Error: invalid quantity of arguments");
    }
}