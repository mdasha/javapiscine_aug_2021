public class Program    {
    public static void main(String[] args) throws  InterruptedException, IllegalNumberOfArguments {
        if (args.length != 1) {
            throw new IllegalNumberOfArguments();
        }

        try {
            String [] countArray = args[0].split("=");
            String countString = countArray[1];
            int count = Integer.parseInt(countString.trim());
            if (count <= 0) {
                System.err.println("Error: count must be positive number.");
                System.exit(-1);
            }
            HenEgg egg = new HenEgg("Egg", count);
            HenEgg hen = new HenEgg("Hen", count);

            egg.start();
            hen.start();
            egg.join();
            hen.join();
            int i = 0;
            while (i < count) {
                System.out.println("Human");
                i++;
            }
        }
        catch (NumberFormatException nfe) {
            System.out.println("Number Format Exception: " + nfe.getMessage());
        }
        System.exit(0);
    }
}