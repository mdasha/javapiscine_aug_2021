import java.util.Scanner;
import java.util.Iterator;
import java.io.*;
import java.util.Map;
import java.util.HashMap;

public class Program {

    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        String fileUrl = "";
        FileOutputStream fout = new FileOutputStream("./result.txt");

        while (true) {
            fileUrl = scan.nextLine();
            if (fileUrl.equals("42")) {
                fout.close();
                break ;
            }
            File fin = new File(fileUrl);
            StringBuilder hexFile = convertToHex(fin);
            String hexFileToString = hexFile.toString();
            Map<String, String> mapSignature = parseSignatures();
            String fileExtension = getExtension(mapSignature, hexFileToString);

            if (!fileExtension.equals("")) {
                System.out.println("PROCESSED");
                String data = fileExtension + "\n";
                fout.write(data.getBytes(), 0, data.length());
            }
            else {
                System.out.println("UNDEFINED");
            }
        }
        System.exit(0);
    }

    public static String getExtension(Map<String, String> mapSignature, String hexFileToString) {
        Iterator<Map.Entry<String, String>> entries = mapSignature.entrySet().iterator();
        String fileExt = "";
        while (entries.hasNext()) {

            Map.Entry<String, String> entry = entries.next();
            String standardSignature =  entry.getKey();
            String fileSignature = hexFileToString.substring(0, entry.getKey().length());

            if (standardSignature.equals(fileSignature)) {
                fileExt = entry.getValue();
                break ;
            }
        }
        return (fileExt);
    }

    public static Map<String, String> parseSignatures() throws IOException {
        Map<String, String> mapSignature = new HashMap<String, String>();
        try (FileInputStream fin = new FileInputStream("./src/signatures.txt")) {
            StringBuilder fileSignatureContents = new StringBuilder();
            int i = -1;
            while ((i = fin.read()) != -1) {
                fileSignatureContents.append((char) i);
            }
            String fileSignatureContentsString = fileSignatureContents.toString();
            String[] fileSignatureLines = fileSignatureContentsString.split("\n");
            i = 0;
            while (i < fileSignatureLines.length) {
                String[] lineArray = fileSignatureLines[i].split(",");
                String value = lineArray[0];
                String key = fileSignatureLines[i].substring(lineArray[0].length() + 2);
                mapSignature.put(key, value);
                i++;
            }
            fin.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return(mapSignature);
    }

    public static StringBuilder convertToHex(File file) throws IOException {
        InputStream is = new FileInputStream(file);
        int bytesCounter = 0;
        int value = 0;
        StringBuilder sbHex = new StringBuilder();
        StringBuilder sbResult = new StringBuilder();
        while ((value = is.read()) != -1) {
            sbHex.append(String.format("%02X ", value));
            if (bytesCounter == 15) {
                sbResult.append(sbHex).append("\n");
                sbHex.setLength(0);
                bytesCounter = 0;
            } else {
                bytesCounter++;
            }
        }
        if (bytesCounter != 0) {
            for (; bytesCounter < 16; bytesCounter++) {
                sbHex.append("   ");
            }
            sbResult.append(sbHex).append("\n");
        }
        StringBuilder deneme = sbResult;
        is.close();
        return deneme;
    }
}