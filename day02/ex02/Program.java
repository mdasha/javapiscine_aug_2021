import java.util.Scanner;
import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Stream;

public class Program {

    public static void main(String[] args) {

        if (args.length < 1) {
            System.err.println("Error: illegal arguments");
            System.exit(-1);
        }

        String arg0 = args[0];
        String [] curDirArray = args[0].split("=");
        String curDir = curDirArray[1];
        String main = curDir;
        System.out.println(curDir);

        Scanner scan = new Scanner(System.in);
        String command = "";

        while (true) {
            command = scan.nextLine();
            if (command.length() < 2) {
                System.err.println("Команда введена неверно");
                continue ;
            }
            if (command.substring(0,2).equals("cd")) {
                curDir = runCdCommand(curDir, command, main);
            }
            else if (command.substring(0,2).equals("ls")) {
                runLsCommand(curDir);
            }
            else if (command.substring(0,2).equals("mv")) {
                runMvCommand(curDir, command);
            }
            else if (command.length() == 4 && command.substring(0,4).equals("exit")) {
               break ;
            }
            else {
                System.err.println("Команда введена неверно");
            }
        }
        System.exit(0);
    }

    public static String runCdCommand(String curDir, String command, String main) {
        String tmp = curDir;
        String [] commandArray = command.split(" ");
        if (commandArray.length == 1 || commandArray[1].equals("/") || commandArray[1].equals("//")) {
            curDir = main;
        }
        else if (commandArray[1].equals("..")) {
            String [] curDirArray = curDir.split("/");
            int curDirCut = curDirArray[curDirArray.length - 1].length() + 1;
            curDir = curDir.substring(0, curDir.length() - curDirCut);
        }
        else if (commandArray[1].substring(0,3).equals("../")) {
            String [] curDirArray = curDir.split("/");
            int curDirCut = curDirArray[curDirArray.length - 1].length();
            curDir = curDir.substring(0, curDir.length() - curDirCut) + commandArray[1].substring(3);
        }
        else {
            if (curDir.charAt(curDir.length() - 1) != '/') {
                curDir = curDir + "/" + commandArray[1] ;
            }
            else {
                curDir = curDir + commandArray[1];
            }
        }
        Path path = Paths.get(curDir);

        if (Files.exists(path)) {
            System.out.println(curDir);
        }
        else {
            System.err.println("Error: folder not found");
            System.out.println(curDir);
            System.out.println(tmp);
            curDir = tmp;
        }
        return curDir;

    }

    public static void runLsCommand(String curDir) {
        File dir = new File(curDir);
        File[] arrFiles = dir.listFiles();
        List<File> lst = Arrays.asList(arrFiles);
        String fileLength = "";
        long length = 0;
        for (File file : lst) {
            if (file.isFile()) {
                length = file.length();
            }
            else {
                Path path = Paths.get(file.getPath());
                try (Stream<Path> walk = Files.walk(path)) {
                    length = walk
                            .filter(Files::isRegularFile)
                            .mapToLong(p -> {
                                try {
                                    return Files.size(p);
                                } catch (IOException e) {
                                    System.out.printf("Невозможно получить размер файла %s%n%s", p, e);
                                    return 0L;
                                }
                            })
                            .sum();
                } catch (IOException e) {
                    System.out.printf("Ошибка при подсчёте размера директории %s", e);
                }
            }
            if (length < 1024)
            {
                fileLength = length + " B";
            }
            else {
                length = length / 1024;
                if (length < 1024) {
                    fileLength = length + " KB";
                }
                else {
                    length = length / 1024;
                    if (length < 1024) {
                        fileLength = length + " MB";
                    }
                    else {
                        length = length / 1024;
                        fileLength = length + " GB";
                    }
                }
            }
            System.out.println(file.getName() + " " + fileLength);
        }
    }

    public static void runMvCommand(String curDir, String command) {
        String [] commandArray = command.split(" ");
        if (commandArray.length < 3) {
            System.out.println("Error: not enougth arguments");
            return ;
        }
        Path path;
        String newPath;
        if (commandArray[2].substring(0,3).equals("../")) {
            String [] curDirArray = curDir.split("/");
            int curDirCut = curDirArray[curDirArray.length - 1].length();
            path = Paths.get(curDir.substring(0, curDir.length() - curDirCut) + commandArray[2].substring(3));
            newPath = curDir.substring(0, curDir.length() - curDirCut) + commandArray[2].substring(3);
        }
        else {
            path = Paths.get(curDir + commandArray[2]);
            newPath = curDir + commandArray[2];
        }

        File file = new File(curDir + "/" + commandArray[1]);
        String source = curDir + "/" + commandArray[1];
        String dest = "";
        if (Files.exists(path)) {
            if (file.exists()) {
                dest = newPath + "/" + commandArray[1];
                moveFile(source, dest);
            }
            else {
                System.out.println("Error: file not found");
            }
        }
        else {
            if (file.exists()) {
                dest = curDir + "/" + commandArray[2];
                moveFile(source, dest);
            }
            else {
                System.out.println("Error: file not found");
            }
        }
    }

    private static void moveFile(String src, String dest ) {
        Path result = null;
        try {
            result =  Files.move(Paths.get(src), Paths.get(dest));
        } catch (IOException e) {
            System.out.println("Exception while moving file: " + e.getMessage());
        }
    }
}