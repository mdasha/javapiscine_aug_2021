import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Program {

    public static void main(String[] args) throws IOException {

        if (args.length < 2) {
            System.err.println("Error: illegal arguments");
            System.exit(-1);
        }

        String fileName = "./"  + args[0];
        String fileName2 = "./" + args[1];

        SortedSet<String> sortedSet = new TreeSet<>();
        sortedSet = readUsingBufferedReader(fileName, StandardCharsets.UTF_8, sortedSet);
        sortedSet = readUsingBufferedReader(fileName2, StandardCharsets.UTF_8, sortedSet);
        Map<String, Integer> textAMap = makeMap(fileName, StandardCharsets.UTF_8);
        Map<String, Integer> textBMap = makeMap(fileName2, StandardCharsets.UTF_8);
        System.out.println("");
        Vector<Integer> vectorA = makeFreqVector(textAMap, sortedSet);
        Vector<Integer> vectorB = makeFreqVector(textBMap, sortedSet);
        System.out.println("List of elements A: " + vectorA);
        System.out.println("List of elements B: " + vectorB);
        double similarity = calculateSimilarity(vectorA, vectorB);
        String formattedDouble = String.format("%.3f", similarity);
        System.out.println("Similarity = " + formattedDouble);
        putDictionaryIntoFile(sortedSet);
        System.out.println();

        System.exit(0);
    }

    public static void putDictionaryIntoFile(SortedSet<String> sortedSet) throws IOException {
        FileOutputStream fout = new FileOutputStream("./dictionary.txt");

        Iterator iterator = sortedSet.iterator();
        int i = 0;
        while(iterator.hasNext()) {
            String element =(String)iterator.next();
            String element2 = ", ";
            fout.write(element.getBytes(), 0, element.length());
        if (i != sortedSet.size() - 1) {
            fout.write(element2.getBytes(), 0, element2.length());
        }
            i++;
        }
        fout.close();
    }

    public static double calculateSimilarity(Vector<Integer> vectorA, Vector<Integer> vectorB) {
        int i = 0;
        double numerator = 0;
        double summaSqA = 0;
        double summaSqB = 0;
        while (i < vectorA.size()) {
            numerator += vectorA.elementAt(i) * vectorB.elementAt(i);
            summaSqA += vectorA.elementAt(i) * vectorA.elementAt(i);
            summaSqB += vectorB.elementAt(i) * vectorB.elementAt(i);
            i++;
        }
        double denominator = Math.sqrt(summaSqA) * Math.sqrt(summaSqB);
        double similarity = numerator / denominator;
        return (similarity);
    }

    public static Vector<Integer> makeFreqVector(Map<String, Integer> textMap, SortedSet<String> sortedSet) {
        Vector<Integer> vector = new Vector<>();
        Iterator iterator = sortedSet.iterator();
        while(iterator.hasNext()) {
            String element = (String) iterator.next();
            Integer result= textMap.get(element);
            if (result != null) {
                vector.add(textMap.get(element));
            }
            else {
                vector.add(0);
            }
        }
        return (vector);
    }

    private static Map<String, Integer> makeMap(String fileName, Charset cs) throws IOException {

        Map<String, Integer> mapWordFreq = new HashMap<String, Integer>();
        File file = new File(fileName);
        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis, cs);
        BufferedReader br = new BufferedReader(isr);
        String line;
        Set<String> set = new HashSet<>();

        while((line = br.readLine()) != null){
            String [] data = line.split(" ");
            int i = 0;
            while (i < data.length) {
                if(!set.add(data[i])) {
                    mapWordFreq.replace(data[i], mapWordFreq.get(data[i])+1);
                }
                else {
                    mapWordFreq.put(data[i], 1);
                }
                i++;
            }
        }
        br.close();
        fis.close();
        isr.close();
        return (mapWordFreq);
    }

    private static SortedSet<String> readUsingBufferedReader(String fileName, Charset cs, SortedSet<String> sortedSet) throws IOException {
        File file = new File(fileName);
        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis, cs);
        BufferedReader br = new BufferedReader(isr);
        String line;

        while((line = br.readLine()) != null){
            String [] data = line.split(" ");
            int i = 0;
            while (i < data.length) {
                sortedSet.add(data[i]);
                i++;
            }
        }
        br.close();
        fis.close();
        isr.close();
        return (sortedSet);
    }
}