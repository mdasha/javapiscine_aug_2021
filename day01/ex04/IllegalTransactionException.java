public class IllegalTransactionException extends Exception {
    public IllegalTransactionException() {
        super("Error: illegal transaction, not enough money");
    }
}