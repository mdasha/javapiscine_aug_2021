import java.util.UUID;

public class TransactionsService {

    private UserList users;
    private UnpairedTransactions unpairedTransactions;

    TransactionsService() {
        users = new UserArrayList();
        unpairedTransactions = new UnpairedTransactions();
    }

    public void addUser(User user) throws UserNotFoundException {
        users.addUser(user);
    }

    public long retrieveUsersBalance(int userId) throws UserNotFoundException {
        return users.retrieveUserById(userId).getBalance();
    }

    public void performTransformTransaction(int recipientId, int senderId, long transactionAmount) throws IllegalTransactionException ,UserNotFoundException {
        User recipient = users.retrieveUserById(recipientId);
        User sender = users.retrieveUserById(senderId);

        if (this.retrieveUsersBalance(sender.getId()) >= transactionAmount) {
            Transaction transSender = new Transaction(sender, recipient, "OUTCOME", -transactionAmount);
            Transaction transRecipient = new Transaction(transSender.getUuid(), sender, recipient, "INCOME", transactionAmount);
            recipient.getUserTransactions().addTransaction(transRecipient);
            recipient.setBalance(recipient.getBalance() + transactionAmount);
            sender.setBalance(sender.getBalance() - transactionAmount);
            sender.getUserTransactions().addTransaction(transSender);
            return ;
        }
        throw new IllegalTransactionException();
    }

    public Transaction [] retrieveUserTransfers (int userId)  throws UserNotFoundException {
        return users.retrieveUserById(userId).getUserTransactions().toArray();
    }

    public void removeTransactionById(int userId, UUID transactionId) throws UserNotFoundException, TransactionNotFoundException {
        User user =  users.retrieveUserById(userId);
        Transaction transaction = user.getUserTransactions().removeTransactionById(transactionId);
        Transaction unpairedTransaction = transaction;
        User user2 = user;
        if (transaction.getTransferCategory().equals("INCOME"))
        {
            user2 = transaction.getSender();
        }
        else if (transaction.getTransferCategory().equals("OUTCOME")) {
            user2 = transaction.getRecipient();
        }
        Transaction [] senderTransactions = user2.getUserTransactions().toArray();
        int i = 0;
        while (i < senderTransactions.length) {
            if (senderTransactions[i].getUuid() == transactionId) {
                unpairedTransaction = senderTransactions[i];
            }
            i++;
        }
        user.setBalance(user.getBalance() - transaction.getTransferAmount());
        unpairedTransactions.addUnpairedTransaction(unpairedTransaction);
    }

    public Transaction [] checkTransactionValidity() {
        return unpairedTransactions.getUnpairedTransactionsArray();
    }
}
