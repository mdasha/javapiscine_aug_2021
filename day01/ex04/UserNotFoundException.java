public class UserNotFoundException extends Exception {
    public UserNotFoundException() {
        super("Error: user with such id has not been found");
    }
}