public class MyLinkedList<T> {
    T value;
    MyLinkedList<T> next;
    MyLinkedList<T> prev;

    public MyLinkedList(T value) {
        this.next = null;
        this.prev = null;
        this.value = value;
    }

    public T getValue() {
        return (this.value);
    }
}