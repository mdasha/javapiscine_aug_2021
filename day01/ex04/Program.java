import java.util.UUID;

class Program {

    public static void main(String[] args) throws UserNotFoundException, TransactionNotFoundException, IllegalTransactionException {

        User daria = new User("Дарья", 50000);
        User nikolay = new User("Коля", 10000);
        User alex = new User("Алексей", 100);

        System.out.println("");
        System.out.println("Added three Users");
        printUserData(daria);
        printUserData(nikolay);
        printUserData(alex);
        System.out.println("");

        TransactionsService transService = new TransactionsService();
        int i = 0;
        transService.addUser(daria);
        transService.addUser(nikolay);
        transService.addUser(alex);
        while (i < 3) {
            System.out.println("User id: " + i + ", user balance: " + transService.retrieveUsersBalance(i));
            i++;
        }
        i = 0;
        while (i < 5) {
            transService.performTransformTransaction(2, 0, 200);
            transService.performTransformTransaction(2, 1, 200);
            i++;
        }

        Transaction [] dariaTrans = transService.retrieveUserTransfers(daria.getId());
        Transaction [] nikolayTrans = transService.retrieveUserTransfers(nikolay.getId());
        Transaction [] alexTrans = transService.retrieveUserTransfers(alex.getId());
        i = 0;
        System.out.println("");
        System.out.println("Daria transactions");
        while (i < dariaTrans.length) {
            printTransaction(dariaTrans[i]);
            i++;
        }
        System.out.println("");

        i = 0;
        System.out.println("Nikolay transactions");
        while (i < nikolayTrans.length) {
            printTransaction(nikolayTrans[i]);
            i++;
        }
        System.out.println("");

        i = 0;
        System.out.println("Alex transactions");
        while (i < alexTrans.length) {
            printTransaction(alexTrans[i]);
            i++;
        }

        System.out.println("");
        System.out.println("User balance after transactions");
        i = 0;
        while (i < 3) {
            System.out.println("User id: " + i + ", user balance: " + transService.retrieveUsersBalance(i));
            i++;
        }

        System.out.println("");
        System.out.println("Transaction to remove: ");
        System.out.print("User: " + alex.getName() + ". ");
        printTransaction(alexTrans[4]);
        System.out.println("");
        transService.removeTransactionById(2, alexTrans[4].getUuid());
        Transaction [] alexTrans2 = transService.retrieveUserTransfers(alex.getId());
        i = 0;
        System.out.println("Alex transactions");
        while (i < alexTrans2.length) {
            printTransaction(alexTrans2[i]);
            i++;
        }
        System.out.println("");
        System.out.println("Alex balance after removing transaction: ");
        System.out.println("User id: " + 2 + ", user balance: " + transService.retrieveUsersBalance(2));


        System.out.println("");
        System.out.println("Transaction to remove: ");
        System.out.print("User: " + daria.getName() + ". ");
        printTransaction(dariaTrans[4]);
        System.out.println("");
        transService.removeTransactionById(0, dariaTrans[4].getUuid());
        Transaction [] dariaTrans2 = transService.retrieveUserTransfers(daria.getId());
        i = 0;
        System.out.println("Alex transactions");
        while (i < dariaTrans2.length) {
            printTransaction(dariaTrans2[i]);
            i++;
        }
        System.out.println("");
        System.out.println("Daria balance after removing transaction: ");
        System.out.println("User id: " + 0 + ", user balance: " + transService.retrieveUsersBalance(0));

        Transaction [] unpairedTransactions = transService.checkTransactionValidity();
        System.out.println("");
        i = 0;
        System.out.println("Unpaired transactions");
        while (i < unpairedTransactions.length) {
            printTransaction(unpairedTransactions[i]);
            i++;
        }
        System.out.println("");
        System.out.println("Incorrect transaction");
        transService.performTransformTransaction(2, 0, 200000000);
        System.out.println("");

        System.exit(0);
    }

    public static void printTransaction(Transaction transaction) {
        System.out.println("Transaction. uuid: " + transaction.getUuid() + ",  sender: " + transaction.getSender().getName() +
                ", recipient: " + transaction.getRecipient().getName() + ", transferCategory: "
                + transaction.getTransferCategory() +
                ", transferAmount: " + transaction.getTransferAmount());
    }

    public static void printUserData(User user) {
        System.out.println("User. id: " + user.getId() + ", name: " + user.getName() + ", balance: " + user.getBalance());
    }

    public static void printUserArrayList(UserArrayList userList) {
        int i = 0;
        while (i < userList.retrieveNumberOfUsers()) {
            System.out.print((i + 1) + ". ");
            printUserData(userList.retrieveUserByIndex(i));
            i++;
        }
    }
}