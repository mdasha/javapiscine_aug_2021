public class UnpairedTransactions {

    private TransactionsLinkedList userTransactions;

    UnpairedTransactions () {
        userTransactions = new TransactionsLinkedList();
    }

    public void addUnpairedTransaction(Transaction transaction) {

        userTransactions.addTransaction(transaction);
    }

    Transaction [] getUnpairedTransactionsArray() {
        return userTransactions.toArray();
    }
}