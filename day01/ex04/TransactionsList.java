import java.util.UUID;

public interface TransactionsList {
    void addTransaction(Transaction transaction);
    Transaction removeTransactionById(UUID uuid) throws TransactionNotFoundException;
    Transaction[] toArray();
}