public class User {
    private int id;
    private String name;
    private long balance;

    User(String name, long balance) {
        this.name = name;
        if (balance >= 0)
            this.balance = balance;
        else
        {
            System.out.println("error: negative balance. Set balance to Zero by default.");
            this.balance = 0;
        }
        id = UserIdsGenerator.getInstance().generateId();
    }

    public String getName() {
        return (this.name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getBalance() {
        return (this.balance);
    }

    public void setBalance(long balance) {
        if (balance >= 0)
            this.balance = balance;
        else
        {
            System.out.println("error: negative balance. Set balance to Zero by default.");
            this.balance = 0;
        }
    }

    public int getId() {
        return (this.id);
    }

    public void setId(int id) {
        this.id = id;
    }
}