import java.util.UUID;

class TransactionsLinkedList implements TransactionsList {

    private     MyLinkedList<Transaction> head;
    private     MyLinkedList<Transaction> tail;
    private int size;

    TransactionsLinkedList() {
        size = 0;
        head = null;
        tail = null;
    }

    @Override
    public void addTransaction(Transaction transaction) {
        if (head == null) {
            head = tail = new MyLinkedList<>(transaction);
        }
        else {
            tail.next = new MyLinkedList<>(transaction);
            tail.next.prev = tail;
            tail = tail.next;
        }
        size++;
    }

    @Override
    public void removeTransactionById(UUID uuid) throws TransactionNotFoundException {
        MyLinkedList<Transaction> tmp = head;
        while (tmp != null) {
            if (tmp.getValue().getUuid().equals(uuid)) {
                if (tmp == head) {
                    head = tmp.next;
                }
                if (tmp.prev != null) {
                    tmp.prev.next = tmp.next;
                }
                if (tmp.next != null) {
                    tmp.next.prev = tmp.prev;
                }
                size--;
                return ;
            }
            tmp = tmp.next;
        }
        throw new TransactionNotFoundException();
    }

    @Override
    public Transaction[] toArray() {
        MyLinkedList<Transaction> tmpList = head;
        Transaction [] transactions = new Transaction[size];

        int i = 0;
        while (tmpList != null) {
            transactions[i] = tmpList.getValue();
            tmpList = tmpList.next;
            i++;
        }
        return transactions;
    }

    public void printTransactionsLinkedList() {
        int i = 0;
        MyLinkedList<Transaction> tmpList = head;
        while (tmpList != null) {
            System.out.print((i + 1) + ". ");
            tmpList.getValue().printTransaction();
            tmpList = tmpList.next;
            i++;
        }
    }

    public void printTransactionsToArray(Transaction[] transToArray) {
        int i = 0;
        while (i < size) {
            System.out.print((i + 1) + ". ");
            transToArray[i].printTransaction();
            i++;
        }
    }
}