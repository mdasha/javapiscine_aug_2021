public class UserIdsGenerator {
    public static UserIdsGenerator instance;
    private static int identifier;

    private UserIdsGenerator() {}

    public static  UserIdsGenerator getInstance() {
        if (instance == null) {
            instance = new UserIdsGenerator();
        }
        return instance;
    }

    public static int generateId() {
        return (identifier++);
    }
}