public class UserArrayList implements UserList {

    private User []   users;
    private int       size;
    private int       capacity;

    UserArrayList() {
        users = new User[10];
        size = 0;
        capacity = 10;
    }

    @Override
    public User retrieveUserById(int id) throws UserNotFoundException {
        int i = 0;
        while (i < size) {
            if(users[i].getId() == id) {
                return (users[i]);
            }
            i++;
        }
        throw new UserNotFoundException();
    }

    @Override
    public User retrieveUserByIndex(int index) {
        return users[index];
    }

    @Override
    public int  retrieveNumberOfUsers() {
        return size;
    }

    public int getCapacity() {
        return capacity;
    }

    @Override
    public void addUser(User user) {
        if (size < capacity) {
            users[size] = user;
            size++;
        }
        else {
            User [] newUsers = new User[capacity + capacity/2];
            int i = 0;
            while (i < size) {
                newUsers[i] = users[i];
                i++;
            }
            newUsers[i] = user;
            users = newUsers;
            size++;
            capacity = capacity + capacity/2;
        }
    }
}