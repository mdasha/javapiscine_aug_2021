public interface UserList {
    void addUser(User user) throws UserNotFoundException;
    User retrieveUserById(int id) throws UserNotFoundException;
    User retrieveUserByIndex(int index);
    int  retrieveNumberOfUsers();
}