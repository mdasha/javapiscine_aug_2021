public class MyLinkedList<T> {
    T value;
    MyLinkedList<T> next;
    MyLinkedList<T> prev;

    public MyLinkedList(T value) {
        this.next = null;
        this.prev = null;
        this.value = value;
    }

    public MyLinkedList(T value, MyLinkedList<T> prev, MyLinkedList<T> next) {
        this.value = value;
        this.next = next;
        this.prev = prev;
    }

    public T getValue() {
        return (this.value);
    }
}