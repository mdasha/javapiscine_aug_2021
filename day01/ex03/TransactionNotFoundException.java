public class TransactionNotFoundException extends Exception {
    public TransactionNotFoundException() {
        super("Error: transaction with such uuid has not been found");
    }
}