import java.util.UUID;

class Program {

    public static void main(String[] args) throws UserNotFoundException, TransactionNotFoundException {

        User daria = new User("Дарья", 50000);
        User nikolay = new User("Коля", 10000);
        User alex = new User("Алексей", 100);

        System.out.println("");
        System.out.println("Added three Users");
        printUserData(daria);
        printUserData(nikolay);
        printUserData(alex);
        System.out.println("");

        Transaction trans1 = new Transaction(nikolay, daria, "INCOME", 5000);
        Transaction trans2 = new Transaction(nikolay, daria, "OUTCOME", -5000);
        Transaction trans3 = new Transaction(daria, alex, "OUTCOME", -5000);
        Transaction trans4 = new Transaction(daria, alex, "INCOME", 5000);

        daria.setBalance(daria.getBalance() + trans1.getTransferAmount());
        nikolay.setBalance(nikolay.getBalance() + trans2.getTransferAmount());
        daria.setBalance(daria.getBalance() + trans3.getTransferAmount());
        alex.setBalance(alex.getBalance() + trans4.getTransferAmount());

        System.out.println("Added four Transactions");
        printTransaction(trans1);
        printTransaction(trans2);
        printTransaction(trans3);
        printTransaction(trans4);
        System.out.println("");

        System.out.println("Users after transactions");
        printUserData(daria);
        printUserData(nikolay);
        printUserData(alex);
        System.out.println("");

        System.out.println("Mistaken inputs");
        System.out.println("User with negative balance:");
        User bankrupt = new User("Банкрот", -50000);
        printUserData(bankrupt);
        System.out.println("");

        System.out.println("Unknown type of transaction:");
        Transaction trans5 = new Transaction(nikolay, daria, "STRANGE", 5000);
        printTransaction(trans5);
        System.out.println("");

        System.out.println("Not enough money for transaction:");
        Transaction trans6 = new Transaction(nikolay, daria, "INCOME", 500000);
        printTransaction(trans6);
        System.out.println("");

        System.out.println("Incorrect transfer amount: ");
        Transaction trans7 = new Transaction(nikolay, daria, "INCOME", -500);
        printTransaction(trans7);
        Transaction trans8 = new Transaction(nikolay, daria, "OUTCOME", 500);
        printTransaction(trans8);
        System.out.println("");

        System.out.println("Adding users to list: ");
        UserArrayList userList1 = new UserArrayList();
        userList1.addUser(daria);
        int i = 0;
        while (i < 15) {
            userList1.addUser(new User("Бот " + (i + 1), 100));
            i++;
        }
        printUserArrayList(userList1);
        System.out.println("capacity: " + userList1.getCapacity() + ", number of Users: " + userList1.retrieveNumberOfUsers());
        System.out.println("");

        System.out.println("Printing User by index: ");
        printUserData(userList1.retrieveUserByIndex(8));
        printUserData(userList1.retrieveUserByIndex(0));
        System.out.println("");

        System.out.println("Printing User by id: ");
        printUserData(userList1.retrieveUserById(8));
        System.out.println("");

        System.out.println("Adding transactions:");
        System.out.println("Linked List transactions:");
        TransactionsLinkedList transactions = new TransactionsLinkedList();
        Transaction [] transToArray;
        transactions.addTransaction(trans1);
        transactions.addTransaction(trans2);
        transactions.addTransaction(trans3);
        transactions.addTransaction(trans4);
        transactions.addTransaction(trans5);
        transactions.addTransaction(trans6);
        transactions.addTransaction(trans7);
        transactions.addTransaction(trans8);
        transactions.printTransactionsLinkedList();
        transToArray = transactions.toArray();
        System.out.println("");

        System.out.println("Linked List To Array transactions:");
        transactions.printTransactionsToArray(transToArray);
        System.out.println("");

        UUID uuid = transToArray[1].getUuid();
        System.out.println("Removing transactions:");
        System.out.println("Transaction to remove:");
        printTransaction(transToArray[1]);
        System.out.println("");
        transactions.removeTransactionById(transToArray[1].getUuid());
        System.out.println("Linked List after removal: ");
        transactions.printTransactionsLinkedList();
        System.out.println("Linked List to Array after removal: ");
        transactions.printTransactionsToArray(transactions.toArray());
        System.out.println("");

        System.out.println("Removing transactions (not found exception):");
        System.out.println("not found uuid: " + uuid);
        transactions.removeTransactionById(uuid);
        System.exit(0);
    }

    public static void printTransaction(Transaction transaction) {
        System.out.println("Transaction. uuid: " + transaction.getUuid() + ",  sender: " + transaction.getSender().getName() +
                ", recipient: " + transaction.getRecipient().getName() + ", transferCategory: "
                + transaction.getTransferCategory() +
                ", transferAmount: " + transaction.getTransferAmount());
    }

    public static void printUserData(User user) {
        System.out.println("User. id: " + user.getId() + ", name: " + user.getName() + ", balance: " + user.getBalance());
    }

    public static void printUserArrayList(UserArrayList userList) {
        int i = 0;
        while (i < userList.retrieveNumberOfUsers()) {
            System.out.print((i + 1) + ". ");
            printUserData(userList.retrieveUserByIndex(i));
            i++;
        }
    }



}