import java.util.UUID;

public class Transaction {
    private UUID uuid;
    private User recipient;
    private User sender;
    private String transferCategory;
    private long transferAmount;

    String INCOME = "INCOME";
    String OUTCOME = "OUTCOME";

    Transaction(User sender, User recipient, String transferCategory, long transferAmount) {

        if (sender.getBalance() < transferAmount) {
            System.err.println("error: transaction is impossible! Not enough money!");
            transferAmount = 0;
        }

        if (!(transferCategory.equals(INCOME) || (transferCategory.equals(OUTCOME)))) {
            System.err.println("error: incorrect type of transaction!");
            transferAmount = 0;
        }

        if (transferCategory.equals(INCOME)) {
            if (transferAmount <= 0) {
                System.err.println("error: transaction, incorrect transfer amount!");
                transferAmount = 0;
            }
        } else if (transferCategory.equals(OUTCOME)) {
            if (transferAmount >= 0) {
                System.err.println("error: transaction, incorrect transfer amount!");
                transferAmount = 0;
            }
        }
        uuid = UUID.randomUUID();
        this.transferCategory = transferCategory;
        this.transferAmount = transferAmount;
        this.sender = sender;
        this.recipient = recipient;
    }

    public UUID getUuid() {
        return (this.uuid);
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public User getRecipient() {
        return (this.recipient);
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    public User getSender() {
        return (this.sender);
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public String getTransferCategory() {
        return (this.transferCategory);
    }

    public void setTransferCategory(String transferCategory) {
        this.transferCategory = transferCategory;
    }

    public long getTransferAmount() {
        return (this.transferAmount);
    }

    public void setTransferAmount(long transferAmount) {
        if (sender.getBalance() < transferAmount) {
            System.err.println("error: transaction is impossible! Not enough money!");
            transferAmount = 0;
        }

        if (!(transferCategory.equals(INCOME) || (transferCategory.equals(OUTCOME)))) {
            System.err.println("error: incorrect type of transaction!");
            transferAmount = 0;
        }

        if (transferCategory.equals(INCOME)) {
            if (transferAmount <= 0) {
                System.err.println("error: transaction, incorrect transfer amount!");
                transferAmount = 0;
            }
        } else if (transferCategory.equals(OUTCOME)) {
            if (transferAmount >= 0) {
                System.err.println("error: transaction, incorrect transfer amount!");
                transferAmount = 0;
            }
        }
        this.transferAmount = transferAmount;
    }
}